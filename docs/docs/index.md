# Uniqueness Point Calculator

This program will calculate the uniqueness points for a specified set of word or phone sequences based on a specific corpus of information. It's written in Python, and it should work with both versions 2 and 3. Output can be written to file or to the console.

Two different methods of calculating the uniqueness point are output. The first is determining at which point there is only one option left for a given sequence, representing when the item in question is expected to be in the base corpus. The second is determining at which point there are no options left for a given sequence, representing when the item in question is not expected to be in the base corpus.

**Proceed to the GitLab repository** [here](), or find the documentation in the menu.

This script is licensed under version [2.1 of the GNU Lesser General Public License](https://www.gnu.org/licenses/old-licenses/lgpl-2.1.en.html).
