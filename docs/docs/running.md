# Running the Script

Provided that you have Python installed, you should be able to run the program with the following command:

> python uniqueness_point.py corpus queries output

where

"corpus" is the name of the file containing the data that program will use to compare all queries to determine their uniqueness point. It should be in a similar format to the CMU pronouncing dictionary wherein the first element of a line is a word and the rest of the items in the line, separated by whitespace, are the phones/graphs that will be used for the comparisons. Any digits indicating stress will be stripped away. An example of a line in this format may be found below:

> PYTHON  P AY1 TH AA0 N

"queries" is the name of the file containing the words/sequences to query. **This is an optional argument.** This file should be formatted similarly to the above but not have the word at the start. Any digits will be stripped from each line. An example of a line in this format may be found below:

> B L IH K

"output" is the name of the file to which the results should be written. **This is an optional argument.** If this argument is not provided, the program will write the results to the console. The format of the output will be in a 3 column CSV file, where the first column is the word that was queried, the second column is the element (indexed at 1) at which the the item matched 0 of the items in the corpus, and the third column is the element (indexed at 1) at which the item matched only 1 of the items in the corpus.
