# uniqueness_point.py

© Matthew C. Kelley, February 2017

This software is licensed under the LGPLv2.1. For more information, please visit [the GNU website](https://www.gnu.org/licenses/old-licenses/lgpl-2.1.en.html).

A Python script to determine the uniqueness points for a desired set of words based on a specified corpus to compare against.

## Node

Represents a node in a tree.

**Attributes:**
* label (`str`): The label the node contains.
* branches (`dict`): The `Node` objects that are connected beneath this `Node`.

### add_branch(label, depth)

Adds a branch to this `Node`.

**Attributes:**
* label (`str`): The label for the new `Node` being added as a branch.
* depth (`int`): How many levels down the tree the `Node` is located.

**Returns:**

`Node`: The `Node` added as a branch.

## Tree

Represents a tree made up of `Node` objects.

**Attributes:**
* root (`Node`): The root of the tree that holds the rest of the `Node` objects.

### add_word(word)

Adds a `Node` to the `Tree` that corresponds to the sequence of elements in the word passed in.

**Attributes:**
* word (`list`): the sequence of elements representing the word to be added.

### uniqueness_point(word, in_lexicon=`True`)

Finds the uniqueness point for a specified word.

**Attributes:**
* word (`list`): The sequence of elements representing the word for which to calculate the uniqueness point.
* in_lexicon (`bool`): Whether or not the word is expected to be in the lexicon or not. If `True`, the uniqueness point will be determined based on when a branch can be considered to represent only a single word. If `False`, the uniqueness point will be determined based on when a branch can no longer be traversed (as might be desired to be determined for nonwords or foreign words).

**Returns:**

`int`: The element at which the word can be determined to be unique (indexed from 1) or `-1` if the word cannot be determined to be unique.
