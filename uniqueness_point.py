# uniqueness_point.py
#
# A Python script to determine the uniqueness points for a desired set of words
# based on a specified corpus to compare against.

from __future__ import print_function
import sys
from string import digits

class Node:
    '''
    Represents a node in a tree.

    Attributes:
        label (`str`): The label the node contains.
        branches (`dict`): The `Node` objects that are connected beneath this
        `Node`.
    '''

    def __init__(self, label, depth):

        self.label = label
        self.branches = {}
        self.depth = depth

    def add_branch(self, label, depth):
        '''
        Adds a branch to this `Node`.

        Attributes:
            label (str): The label for the new `Node` being added as a branch.
            depth (int): How many levels down the tree the `Node` is located.

        Returns:
            Node: The `Node` added as a branch.
        '''

        self.branches[label] = Node(label, depth)
        return self.branches[label]

    def __str__(self):

        return self.label

    __repr__ = __str__

class Tree:
    '''
    Represents a tree made up of `Node` objects.

    Attributes:
        root (`Node`): The root of the tree that holds the rest of the `Node`
        objects.
    '''

    def __init__(self):

        self.root = Node(None, 1)

    def add_word(self, word):
        '''
        Adds a `Node` to the `Tree` that corresponds to the sequence of elements in
        the word passed in.

        Attributes:
            word (`list`): the sequence of elements representing the word to be
            added.
        '''

        curr = self.root

        for i in word:

            if i not in curr.branches:

                curr = curr.add_branch(i, curr.depth + 1)

            else:

                curr = curr.branches[i]

    def uniqueness_point(self, word, in_lexicon=True):
        '''
        Finds the uniqueness point for a specified word.

        Attributes:
            word (`list`): The sequence of elements representing the word for
            which to calculate the uniqueness point.

            in_lexicon (`bool`): Whether or not the word is expected to be in the
            lexicon or not. If `True`, the uniqueness point will be determined
            based on when a branch can be considered to represent only a single
            word. If `False`, the uniqueness point will be determined based on
            when a branch can no longer be traversed (as might be desired to be
            determined for nonwords or foreign words).

        Returns:
            `int`: The element at which the word can be determined to be unique
            (indexed from 1) or `-1` if the word cannot be determined to be
            unique.
        '''

        curr = self.root
        
        if word[0] not in curr.branches:
            
            return 0

        for i in word:

            # found uniqueness point
            if i not in curr.branches:

                return curr.depth

            # check if remaining branch only represents one word
            elif len(curr.branches) == 1 and in_lexicon:

                j = curr.branches[i]

                # descend tree and check that there's only one branch from here
                num_branches_from_here = len(j.branches)

                while num_branches_from_here <= 1:

                    if num_branches_from_here == 0:
                        return curr.depth
                    else:

                        j = j.branches[list(j.branches.keys())[0]]
                        num_branches_from_here = len(j.branches)

                curr = curr.branches[i]

            else:

                curr = curr.branches[i]

        # never unique
        if curr.depth > len(word) and not curr.branches == dict():
            return -1

        if curr.branches == dict():
            return curr.depth - 1
        else:
            return curr.depth

def main():

    corpus = sys.argv[1]

    if len(sys.argv) >= 3:
        queries = sys.argv[2]
    else:
        queries = corpus

    if len(sys.argv) >= 4:
        f = open(sys.argv[3], 'w')
    else:
        f = sys.stdout

    # version checking for compatibility for Python 2 and Python 3
    using_3 = sys.version_info[0] == 3

    tree = Tree()

    if using_3:
        strip_nums = str.maketrans('', '', digits)

    for line in open(corpus, 'r'):

        if using_3:
            word = line.strip().translate(strip_nums).split()[1:]
        else:
            word = line.strip().translate(None, digits).split()[1:]

        tree.add_word(word)

    print('{},{},{}'.format('Word', 'Uniquess_point_in_lex',
        'Uniqueness_point_out_lex'), file=f)

    for line in open(queries):

        if using_3:
            word = line.strip().translate(strip_nums).split()
        else:
            word = line.strip().translate(None, digits).split()
            
        up_in = tree.uniqueness_point(word)
        up_out = tree.uniqueness_point(word, in_lexicon=False)
        s = '{},{},{}'.format(' '.join(word), up_in, up_out)

        print(s, file=f)

    f.close()

if __name__ == '__main__': main()
